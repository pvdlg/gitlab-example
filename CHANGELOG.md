<a name="1.0.0"></a>
# 1.0.0 (2018-01-02)


### Features

* initial release ([d6df1b3](https://gitlab.com/pvdlg/gitlab-example/commit/d6df1b3))

<a name="1.0.0"></a>
# 1.0.0 (2018-01-01)


### Features

* initial release ([09f98f3](https://gitlab.com/pvdlg/semantic-release-example/commit/09f98f3))
